# Xen RISC-V development environment

The purpose of the repo is providing of development environment to develop <br />
RISC-V support for Xen.

# Usage
* make -> fetch docker image, sources (Xen, Linux, QEMU) and build development <br />
        environment <br />
* make run -> Run built development environment
