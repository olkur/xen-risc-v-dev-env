#!/bin/sh

# echo "NOTIFICATION: some commands will run with sudo"

initrd_img_path=$1 # src/busybox/initrd.img
busybox_install_path=$2 # src/busybox/_install
rcS_path=$3

echo $initrd_img_path
echo $busybox_install_path

mkdir -p ${busybox_install_path}/etc/init.d/
cp $rcS_path ${busybox_install_path}/etc/init.d/
mkdir -p ${busybox_install_path}/proc
mkdir -p ${busybox_install_path}/sys
genext2fs -b 6500 -N 1024 -U -d ${busybox_install_path} ${initrd_img_path}

#dd if=/dev/zero of=$initrd_img_path bs=1024 count=4000
#dev_loop=$(losetup --show -f $initrd_img_path)
#mke2fs $dev_loop
#mkdir -p temp
#mount $dev_loop temp/
#cp -r $busybox_install_path/* temp/
#mkdir -p temp/etc/init.d/
#mkdir -p temp/proc temp/sys
#cp $rcS_path temp/etc/init.d/
#umount temp/
#losetup -d $dev_loop
#rm -rf temp
