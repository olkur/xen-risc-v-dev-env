#!/bin/bash

XEN_START_ADDRESS=0x80200000

${1} \
	-ex "target remote 172.19.0.2:1234" \
	-ex "add-symbol-file src/xen/xen/xen-syms ${XEN_START_ADDRESS}" \
	-ex "hb *${XEN_START_ADDRESS}" \
	-ex "file src/xen/xen/xen-syms" \
        -ex "dir src/xen/xen/arch/riscv" \
	-ex "c"
