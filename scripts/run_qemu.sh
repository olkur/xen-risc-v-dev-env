#!/bin/bash

set -e
set -x

rootfs_img=./src/busybox/initrd.img
qemu=./src/qemu/build/qemu-system-riscv64
kernel=./src/linux/arch/riscv/boot/Image.gz
opensbi=./src/opensbi/build/platform/generic/firmware/fw_dynamic.bin
kernel_size=$(cat ${kernel} | wc -c)
xen=./src/xen/xen/xen
# dtb=src/dtbs/riscv64-guest.dtb

page_up() {
    local out
    local alignment=0x1000
    let mask=$((${alignment} - 1))
    let mask=$((~${mask}))
    let out=$(($1 + ${alignment} * 2))
    let out=$((${out} & ${mask}))
    printf "0x%02lx" ${out}
}

kernel_start=0x80400000
kernel_end=$((${kernel_start} + ${kernel_size}))
# dtb_start=$(page_up ${kernel_end})
# dtb_size=$(cat ${dtb} | wc -c)
# dtb_end=$((${dtb_start} + ${dtb_size}))
rootfs_start=$(page_up ${kernel_end})

# printf "\nLaunching qemu-system-riscv64 with Boot Modules:\n"
# printf "\tKernel: 0x%02x -> 0x%02x\n" ${DOM0_KERNEL} ${kernel_end}
# printf "\tDTB:    0x%02x -> 0x%02x\n" ${dtb_start} ${dtb_end}
printf "\tROOTFS_START: 0x%02x" ${rootfs_start}

wait_gdb=""
if [ x$1 = xwait_gdb ]; then
	wait_gdb="-s -S"
fi

${qemu} -M virt \
	-smp 1 \
	-nographic \
	-m 2g \
	-kernel ${xen} \
	-device guest-loader,kernel=${kernel},addr=${kernel_start},bootargs="rw root=/dev/ram console=hvc0 keep_bootcon bootmem_debug debug" \
	-device guest-loader,initrd=${rootfs_img},addr=${rootfs_start} \
	-bios ${opensbi} \
	${wait_gdb}

# qemu-system-riscv64 \
#	-cpu rv64,x-h=true \
#	-M virt -m 1G -display none \
#    -smp 1 \
#	-serial stdio \
#    -monitor telnet::45454,server,nowait  \
#	-device loader,file=$(readlink -e ${xen}),addr=0x80200000 \
#	-bios "$(readlink -e ${opensbi})" \
#    -d in_asm -D qemu_log.txt -s -S # -machine dumpdtb=riscv64-qemu-virt.dtb

