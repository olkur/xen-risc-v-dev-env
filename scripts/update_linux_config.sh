#!/bin/bash
cd $1
sleep 1
sed "/CONFIG_BLK_DEV_RAM\b/s/.*/CONFIG_BLK_DEV_RAM=y/" -i .config
echo "CONFIG_BLK_DEV_RAM_COUNT=16" >> .config
echo "CONFIG_BLK_DEV_RAM_SIZE=2147483648" >> .config
