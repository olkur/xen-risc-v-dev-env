FROM ubuntu:20.04
LABEL maintainer.name="The Xen Project " \
      maintainer.email="xen-devel@lists.xenproject.org"

ENV DEBIAN_FRONTEND=noninteractive
ENV USER root

RUN mkdir /build
WORKDIR /build

RUN apt-get update && \
    apt-get --quiet --yes install \
        build-essential \
        gcc-riscv64-linux-gnu \
        bison \
        flex \
        python3-dev \
        bc \
        cpio \
        genext2fs \
        libpixman-1-dev \
        libpng-dev \
        libglib2.0-dev \
        libaio-dev \
        git \
        ninja-build \
        libncurses-dev \
        && \
        apt-get autoremove -y && \
        apt-get clean && \
        rm -rf /var/lib/apt/lists* /tmp/* /var/tmp/* \

