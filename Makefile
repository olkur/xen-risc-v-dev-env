CROSS_COMPILE := riscv64-linux-gnu-

LINUX_SRC_DIR := src/linux
QEMU_SRC_DIR := src/qemu
XEN_SRC_DIR := src/xen
TOOLCHAIN_SRC_DIR := src/toolchain
DOCKER_SRC_DIR := src/docker
BUSYBOX_SRC_DIR := src/busybox
OPENSBI_SRC_DIR := src/opensbi
SRC_DIRS := ${QEMU_SRC_DIR} ${XEN_SRC_DIR} ${LINUX_SRC_DIR} ${TOOLCHAIN_SRC_DIR} ${BUSYBOX_SRC_DIR} ${OPENSBI_SRC_DIR} ${DOCKER_SRC_DIR}

QEMU_BIN := ${QEMU_SRC_DIR}/build/qemu-system-riscv64
XEN_BIN := ${XEN_SRC_DIR}/xen/xen
LINUX_BIN := ${LINUX_SRC_DIR}/vmlinux
BUSYBOX_BIN := ${BUSYBOX_SRC_DIR}/busybox
OPENSBI_BIN := ${OPENSBI_SRC_DIR}/opensbi
BINARIES := $(QEMU_BIN) $(XEN_BIN) $(LINUX_BIN) ${BUSYBOX_BIN} ${OPENSBI_BIN}

CLONED_DEPS := $(foreach src_dir, $(SRC_DIRS),$(src_dir)/.cloned)

DOCKER_IMAGE_TAG := ${USER}/cross-compile-riscv64
DOCKERFILE := ${DOCKER_SRC_DIR}/riscv64_ubuntu.dockerfile

RISCV_TOOLCHAIN_LINK := https://toolchains.bootlin.com/downloads/releases/toolchains/riscv64-lp64d/tarballs/riscv64-lp64d--glibc--stable-2022.08-1.tar.bz2
TOOLCHAIN_DIR_NAME := riscv64_toolchain
TOOLCHAIN_TAR_NAME := ${TOOLCHAIN_DIR_NAME}.tar.bz2
TOOLCHAIN_DIR_NAME := riscv64_toolchain

.PHONY: all
all: $(CLONED_DEPS) ${XEN_BIN} $(BINARIES)
	@echo "Development environment is ready"

include Makefile.prepare_env
include Makefile.build_env
include Makefile.run_env
